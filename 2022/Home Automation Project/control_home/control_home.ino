/*
 * control_home v.0.0.1
 * Home automation system firmware for controlling applications
 * and control via bluetooth communication
 */
#include<SoftwareSerial.h>

#define BT_RX 2 //software serial rx pin
#define BT_TX 4 //software serial tx pin

#define LOAD1 3 //load 1st
#define LOAD2 5 //load 2nd
#define LOAD3 6 //load 3rd
#define LOAD1_PWM 9 //load 4th
#define LOAD2_PWM 10  //load 5th
#define LOAD3_PWM 11 //load 6th

#define LOAD4 7 //simple load
#define LOAD5 8 //simple load
#define LOAD6 12  //simple load

bool authFlag = false;
bool brightnessFlag = false;
bool load1BrightnessFlag = false;
bool load2BrightnessFlag = false;
bool load3BrightnessFlag = false;

int allLoad[] = {LOAD1, LOAD2, LOAD3, LOAD4, LOAD5, LOAD6};  //all load as a array
#define default_pwm_value 150 //default value for pwm
int pwm_user_value = 0;
int LOAD1_PWM_VALUE = 0;
int LOAD2_PWM_VALUE = 0;
int LOAD3_PWM_VALUE = 0;

//bluetooth communication to HC-05
SoftwareSerial mySerial(BT_RX, BT_TX); //RX, TX

int getByte;
void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);

  pinMode(LOAD1, OUTPUT);
  pinMode(LOAD2, OUTPUT);
  pinMode(LOAD3, OUTPUT);
  pinMode(LOAD4, OUTPUT);
  pinMode(LOAD5, OUTPUT);
  pinMode(LOAD6, OUTPUT);

  analogWrite(LOAD1_PWM, default_pwm_value);
  analogWrite(LOAD2_PWM, default_pwm_value);
  analogWrite(LOAD3_PWM, default_pwm_value);
}

void loop() {
  if(mySerial.available() > 0){
    getByte = mySerial.read() - '0';
    Serial.println(getByte);
    
    //login authentication level
    if(getByte == 64){  //p
      authFlag = true;
      brightnessFlag = false;
      mySerial.write("\n Login OK");
      delay(1000);
    }
    //logout level
    if(getByte == 60){  //l
      authFlag = false;
      mySerial.write("\n Logout OK");
    }
    
    //brightness control level
    if(brightnessFlag){
      brightness(getByte); 
    }

    //load control level
    if(authFlag){
      homeControl(getByte);
    }
  }

}

//this function is set to control the home light and loads via phones
void homeControl(int i){
  switch(i){
    case 1:
      Serial.println("Load 1st turned toggle. ");
      digitalWrite(LOAD1, !digitalRead(LOAD1)); //toggling load 1
      mySerial.write("\n LOAD1 : ");
      if(digitalRead(LOAD1))mySerial.write("ON");
      else mySerial.write("OFF");
      load1BrightnessFlag = true;
      load2BrightnessFlag = false;
      load3BrightnessFlag = false;
      
      break;
    case 2:
      Serial.println("Load 2nd turned toggle. ");
      digitalWrite(LOAD2, !digitalRead(LOAD2)); //toggling load 2
      mySerial.write("\n LOAD2 : ");
      if(digitalRead(LOAD2))mySerial.write("ON");
      else mySerial.write("OFF");
      load1BrightnessFlag = false;
      load2BrightnessFlag = true;
      load3BrightnessFlag = false;
      
      break;
    case 3:
      Serial.println("Load 3rd turned toggle. ");
      digitalWrite(LOAD3, !digitalRead(LOAD3)); //toggling load 3
      mySerial.write("\n LOAD3 : ");
      if(digitalRead(LOAD3))mySerial.write("ON");
      else mySerial.write("OFF");
      load1BrightnessFlag = false;
      load2BrightnessFlag = false;
      load3BrightnessFlag = true;
      
      break;
    case 4:
      Serial.println("Load 4th turned toggle. ");
      digitalWrite(LOAD4, !digitalRead(LOAD4)); //toggling load 4
      mySerial.write("\n LOAD4 : ");
      if(digitalRead(LOAD4))mySerial.write("ON");
      else mySerial.write("OFF");
      break;
    case 5:
      Serial.println("Load 5th turned toggle. ");
      digitalWrite(LOAD5, !digitalRead(LOAD5)); //toggling load 5
      mySerial.write("\n LOAD5 : ");
      if(digitalRead(LOAD5))mySerial.write("ON");
      else mySerial.write("OFF");
      break;
    case 6:
      Serial.println("Load 6th turned toggle. ");
      digitalWrite(LOAD6, !digitalRead(LOAD6)); //toggling load 6
      mySerial.write("\n LOAD6 : ");
      if(digitalRead(LOAD6))mySerial.write("ON");
      else mySerial.write("OFF");
      break;
    case 7:
      Serial.println("all load on. ");
      for(int j=0;j<=5;j++){
        digitalWrite(allLoad[j], HIGH);
        Serial.print("LOAD");
        Serial.print(j+1);
        Serial.print(" : ");
        if(digitalRead(allLoad[j]))Serial.println("ON");
      }
      mySerial.write("\n ALL ON");
      break;
    case 8:
      Serial.println("all load off. ");
      for(int j=0;j<=5;j++){
        digitalWrite(allLoad[j],LOW);
        Serial.print("LOAD");
        Serial.print(j+1);
        Serial.print(" : ");
        if(!digitalRead(allLoad[j]))Serial.println("OFF");
      }
      mySerial.write("\n ALL OFF");
      break;
    case 9:
      Serial.println("Brightness control");
      authFlag = false;
      brightnessFlag = true;
      mySerial.write("\n brightness control 1-9 for 11%-100%");
      break;
    default:
      Serial.println("Invalid information.");
      break;
  }
}

//this function is set to control the brightness of the load
void brightness(int p){
  switch(p){
    case 1:
      pwm_user_value = 50;
      mySerial.write("\n 11%");
      break;
    case 2:
      pwm_user_value = 75;
      mySerial.write("\n 22%");
      break;
    case 3:
      pwm_user_value = 100;
      mySerial.write("\n 33%");
      break;
   case 4:
      pwm_user_value = 125;
      mySerial.write("\n 44%");
      break;
   case 5:
      pwm_user_value = 150;
      mySerial.write("\n 55%");
      break;
   case 6:
      pwm_user_value = 175;
      mySerial.write("\n 66%");
      break;
   case 7:
      pwm_user_value = 200;
      mySerial.write("\n 77%");
      break;
   case 8:
      pwm_user_value = 225;
      mySerial.write("\n 88%");
      break;
   case 9:
      pwm_user_value = 255;
      mySerial.write("\n 100%");
      break;
   default:
      mySerial.write("\n Brightness control exit");
      brightnessFlag = false;
      break;
  }
  if(load1BrightnessFlag){
    LOAD1_PWM_VALUE = pwm_user_value;;
    analogWrite(LOAD1_PWM, LOAD1_PWM_VALUE);
  }
  if(load2BrightnessFlag){
    LOAD2_PWM_VALUE = pwm_user_value;;
    analogWrite(LOAD2_PWM, LOAD2_PWM_VALUE);
  }
  if(load1BrightnessFlag){
    LOAD3_PWM_VALUE = pwm_user_value;;
    analogWrite(LOAD3_PWM, LOAD3_PWM_VALUE);
  }
}
