

/*
 * control_home v.0.0.1
 * Home automation system firmware for controlling applications
 * and control via bluetooth communication
 */
#include "DFRobotDFPlayerMini.h"
#include<SoftwareSerial.h>

#define BT_RX 2 //software serial rx pin
#define BT_TX 4 //software serial tx pin

#define MP3_TX 11 //tx pin

#define LOAD1 5 //load 1st
#define LOAD2 6 //load 2nd
#define LOAD3 7 //load 3rd
#define LOAD4 8 //load 4th
#define LOAD5 9 //load 5th
#define LOAD6 10  //load 6th for battery charger
#define BATT A0

bool authFlag = false;

int allLoad[] = {LOAD1, LOAD2, LOAD3, LOAD4, LOAD5};  //all load as a array

//bluetooth communication to HC-05
SoftwareSerial mySerial(BT_RX, BT_TX); //RX, TX
DFRobotDFPlayerMini myDFPlayer;

//MP3 player object
//SoftwareSerial mp3Serial(MP3_RX, MP3_TX);

int getByte;
void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);
//  mp3Serial.begin(9600);
//  myDFPlayer.begin(mp3Serial);
//  myDFPlayer.setTimeOut(500); //Set serial communictaion time out 500ms
//
//  myDFPlayer.volume(30);
//  myDFPlayer.EQ(DFPLAYER_EQ_NORMAL);
//  myDFPlayer.play(1);
//  myDFPlayer.outputDevice(DFPLAYER_DEVICE_SD);
  pinMode(LOAD1, OUTPUT);
  pinMode(LOAD2, OUTPUT);
  pinMode(LOAD3, OUTPUT);
  pinMode(LOAD4, OUTPUT);
  pinMode(LOAD5, OUTPUT);
  pinMode(LOAD6, OUTPUT);
  pinMode(BATT, INPUT);
  digitalWrite(LOAD1, LOW);
  digitalWrite(LOAD2, LOW);
  digitalWrite(LOAD3, LOW);
  digitalWrite(LOAD4, LOW);
  digitalWrite(LOAD5, HIGH);
  digitalWrite(LOAD6, HIGH);
}

void loop() {
  int batt_perc;
//  myDFPlayer.randomAll(); //Random play all the mp3.
  delay(1000);
  Serial.println(analogRead(BATT));
  batt_perc = map(analogRead(BATT),0,570,0,100);
  Serial.print("BATTERY: ");
  if(batt_perc <= 50){
    digitalWrite(LOAD6, LOW);
    mySerial.write("\n CHARGING.");
  }
  else if(batt_perc > 50 && batt_perc >= 95){
    digitalWrite(LOAD6, HIGH);
//    mySerial.write("\n CHARGE FULL.");
  }
  
  Serial.println(" %");
  delay(1000);
  if(mySerial.available() > 0){
    getByte = mySerial.read() - '0';
    Serial.println(getByte);
    
    //login authentication level
    if(getByte == 64){  //p
      authFlag = true;
      mySerial.write("\n Login OK");
      delay(1000);
    }
    //logout level
    if(getByte == 60){  //l
      authFlag = false;
      mySerial.write("\n Logout OK");
    }
    
    //load control level
    if(authFlag){
      homeControl(getByte);
    }
  }
//  else homeControl(7);/

}

//this function is set to control the home light and loads via phones
void homeControl(int i){
  switch(i){
    case 1:
      Serial.println("Load 1st turned toggle. ");
      digitalWrite(LOAD1, !digitalRead(LOAD1)); //toggling load 1
      mySerial.write("\n Kitchen_DC : ");
      if(digitalRead(LOAD1))mySerial.write("OFF");
      else mySerial.write("ON");
      
      break;
    case 2:
      Serial.println("Load 2nd turned toggle. ");
      digitalWrite(LOAD2, !digitalRead(LOAD2)); //toggling load 2
      mySerial.write("\n OUT_AC : ");
      if(digitalRead(LOAD2))mySerial.write("OFF");
      else mySerial.write("ON");
      
      break;
    case 3:
      Serial.println("Load 3rd turned toggle. ");
      digitalWrite(LOAD3, !digitalRead(LOAD3)); //toggling load 3
      mySerial.write("\n OUT_DC : ");
      if(digitalRead(LOAD3))mySerial.write("OFF");
      else mySerial.write("ON");
      
      break;
    case 4:
      Serial.println("Load 4th turned toggle. ");
      digitalWrite(LOAD4, !digitalRead(LOAD4)); //toggling load 4
      mySerial.write("\n ROOM_DC : ");
      if(digitalRead(LOAD4))mySerial.write("OFF");
      else mySerial.write("ON");
      break;
    case 5:
      Serial.println("Load 5th turned toggle. ");
      digitalWrite(LOAD5, !digitalRead(LOAD5)); //toggling load 5
      mySerial.write("\n ROOM_AC : ");
      if(digitalRead(LOAD5))mySerial.write("OFF");
      else mySerial.write("ON");
      break;
    case 6:
      Serial.println("Load 6th turned toggle. ");
      digitalWrite(LOAD6, !digitalRead(LOAD6)); //toggling load 6
      mySerial.write("\n CHARGE : ");
      if(digitalRead(LOAD6))mySerial.write("OFF");
      else mySerial.write("ON");
      break;
    case 7:
      Serial.println("all load on. ");
      for(int j=0;j<=5;j++){
        digitalWrite(allLoad[j], HIGH);
        Serial.print("LOAD");
        Serial.print(j+1);
        Serial.print(" : ");
        if(digitalRead(allLoad[j]))Serial.println("ON");
      }
      mySerial.write("\n ALL ON");
      break;
    case 8:
      Serial.println("all load off. ");
      for(int j=0;j<=5;j++){
        digitalWrite(allLoad[j],LOW);
        Serial.print("LOAD");
        Serial.print(j+1);
        Serial.print(" : ");
        if(!digitalRead(allLoad[j]))Serial.println("OFF");
      }
      mySerial.write("\n ALL OFF");
      break;
    default:
      Serial.println("Invalid information.");
      break;
  }
}
