/*
 * Gate automation Firmware V1.0
 * may 14 2019
 * (c)bRood Int Technologies
 * 
 */


 // ------------------- Library Initialization ----------------------
 
 #include <WiFi.h>
 #include "Adafruit_MQTT.h"
 #include "Adafruit_MQTT_Client.h"

/************************* WiFi Access Point *********************************/

 #define WLAN_SSID       "__LAB__"
 #define WLAN_PASS       "lab@devil"

/************************* Adafruit.io Setup *********************************/

 #define AIO_SERVER      "io.adafruit.com"
 #define AIO_SERVERPORT  1883                   // use 8883 for SSL
 #define AIO_USERNAME    "devil_edward"
 #define AIO_KEY         "8fe26bd9722447e8bae583b4f8043fe5"

// Create an ESP32 WiFiClient class to connect to the MQTT server.
 WiFiClient client;

 // Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish GATE_STATUS_FEED = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/GATE_STATUS");
// Setup a feed called 'onoff' for subscribing to changes.
Adafruit_MQTT_Subscribe SLIDE_GATE_FEED = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/GATE/GATE_SL", MQTT_QOS_1);
Adafruit_MQTT_Subscribe SWING_GATE_FEED = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/GATE/GATE_SW", MQTT_QOS_1);
Adafruit_MQTT_Subscribe BOTH_GATE_FEED = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/GATE/GATE_BOTH", MQTT_QOS_1);

 // ------------------- Pin Definitions ----------------------------
 const byte RF_D0_INTR = 32;
 const byte RF_D1_INTR = 33;
 const byte RF_D2_INTR = 25;
 const byte RF_D3_INTR = 26;
 const byte RF_VT_INTR = 27;
 const byte VIDEO_DOOR_INTR = 35;
 // ---------------------- --------------------

 #define BUZZER 18

 #define SL_OPEN 12
 #define SL_CLOSE 13
 #define SL_STOP 23

 #define SW_OPEN 22
 #define SW_CLOSE 21
 #define SW_STOP 19
 
// ------------------------ Flag creations ------------------------
 bool RF_D0_FLAG  = false;
 bool RF_D1_FLAG = false;
 bool RF_D2_FLAG = false;
 bool RF_D3_FLAG = false;
 bool VIDEO_DOOR_FLAG = false;
 bool LOCK_FLAG = true;
// ------------------------ Counting Variables -------------------
 int slideCount = 0;
 int swingCount = 0;
 int bothCount = 0;
 int lockCount = 0;
 int childGateCount = 0;

// ---------------------  Interrupt Service routine Definitions -------------
void IRAM_ATTR slideOperate() {
  RF_D0_FLAG = true;
  if(slideCount == 3){
    slideCount = 0;
  }
  slideCount++;
  swingCount = 0;
  bothCount = 0;
  lockCount = 0;
  Serial.print("Slide pressed : ");
  Serial.println(slideCount);
}
 
void IRAM_ATTR swingOperate() {
  RF_D1_FLAG = true;
  if(swingCount == 3){
    swingCount = 0;
  }
  swingCount++;
  slideCount = 0;
  bothCount = 0;
  lockCount = 0;
  Serial.print("Swing pressed : ");
  Serial.println(swingCount);
}

void IRAM_ATTR bothOperate() {
  RF_D2_FLAG = true;
  if(bothCount == 3){
    bothCount = 0;
  }
  bothCount++;
  slideCount = 0;
  swingCount = 0;
  lockCount = 0;
  Serial.print("Both pressed : ");
  Serial.println(bothCount);
}

void IRAM_ATTR lockOperate() {
  RF_D3_FLAG = true;
  if(lockCount == 5){
    lockCount = 0;
  }
  lockCount++;
  slideCount = 0;
  swingCount = 0;
  bothCount = 0;
  Serial.print("Lock pressed : ");
  Serial.println(lockCount);
}

void IRAM_ATTR childGateOperate() {
  VIDEO_DOOR_FLAG = true;
  if(childGateCount == 3){
      childGateCount = 0;
  }
  childGateCount++;
  Serial.println("Child gate pressed");
}

void MQTT_connect();

void setup() {
  //input pin definitions
 pinMode(RF_D0_INTR, INPUT_PULLUP);
 pinMode(RF_D1_INTR, INPUT_PULLUP);
 pinMode(RF_D2_INTR, INPUT_PULLUP);
 pinMode(RF_D3_INTR, INPUT_PULLUP);
 pinMode(RF_VT_INTR, INPUT_PULLUP);
 pinMode(VIDEO_DOOR_INTR, INPUT_PULLUP);

 //output pin definitions
 pinMode(BUZZER, OUTPUT);
 
 pinMode(SL_OPEN, OUTPUT);
 pinMode(SL_CLOSE, OUTPUT);
 pinMode(SL_STOP, OUTPUT);
 
 pinMode(SW_OPEN, OUTPUT);
 pinMode(SW_CLOSE, OUTPUT);
 pinMode(SW_STOP, OUTPUT);

 digitalWrite(SL_OPEN, LOW);
 digitalWrite(SL_CLOSE, LOW);
 digitalWrite(SL_STOP, LOW);
 digitalWrite(SL_OPEN, LOW);
 digitalWrite(SL_CLOSE, LOW);
 digitalWrite(SL_STOP, LOW);
 
 Serial.begin(115200);
 //attching interrupt pins with respected ISRs
 attachInterrupt(digitalPinToInterrupt(RF_D0_INTR), slideOperate, RISING);
 attachInterrupt(digitalPinToInterrupt(RF_D1_INTR), swingOperate, RISING);
 attachInterrupt(digitalPinToInterrupt(RF_D2_INTR), bothOperate, RISING);
 attachInterrupt(digitalPinToInterrupt(RF_D3_INTR), lockOperate, RISING);
 attachInterrupt(digitalPinToInterrupt(VIDEO_DOOR_INTR), childGateOperate, RISING);
 
// Connect to WiFi access point.
  Serial.println(); Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WLAN_SSID);

  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");
  digitalWrite(BUZZER, HIGH);
  delay(100);
  digitalWrite(BUZZER, LOW);
  delay(200);
  digitalWrite(BUZZER, HIGH);
  delay(100);
  digitalWrite(BUZZER, LOW);
  
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for GATE Operation
  mqtt.subscribe(&SLIDE_GATE_FEED);
  mqtt.subscribe(&SWING_GATE_FEED);
  mqtt.subscribe(&BOTH_GATE_FEED);

}

void buzzOpening(int x){
  for(int i = 0; i <= x; i++ ){
      digitalWrite(BUZZER, HIGH);
      Serial.print(".");
      delay(200);
      digitalWrite(BUZZER, LOW);
      delay(50);
      Serial.println("");
    }
}

void buzzStoping(int y){
  for(int i = 0; i <= y; i++ ){
      digitalWrite(BUZZER, HIGH);
      Serial.print("-");
      delay(100);
      digitalWrite(BUZZER, LOW);
      delay(100);
      Serial.println("");
    }
}

void buzzClosing(int y){
  for(int i = 0; i <= y; i++ ){
      digitalWrite(BUZZER, HIGH);
      Serial.print("-");
      delay(50);
      digitalWrite(BUZZER, LOW);
      delay(200);
      Serial.println("");
    }
}

void checkPin(int z){
  Serial.print("Pin ");
  Serial.print(z);
  Serial.println("is High");
  digitalWrite(z, HIGH);
  buzzOpening(10);
  Serial.print("Pin ");
  Serial.print(z);
  Serial.println("is Low");
  digitalWrite(z, LOW);
  buzzClosing(5);
}

// ------------------------            MAIN LOOOOOOOOOOOOOP ----------------
void loop() {
  MQTT_connect();
  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(500))) {
    if (subscription == &SLIDE_GATE_FEED) {
      Serial.print(F("Slide gate command Got: "));
      Serial.println((char *)SLIDE_GATE_FEED.lastread);
      RF_D0_FLAG = true;
      slideCount++;
      LOCK_FLAG = false;
      if(slideCount == 4){
        LOCK_FLAG = true;
        slideCount = 0;
        if(!GATE_STATUS_FEED.publish("Gate : Locked")){
          Serial.println("Failed");
        }else Serial.println("OK");
        digitalWrite(BUZZER, HIGH);
        delay(2000);
        digitalWrite(BUZZER, LOW);
      }
      continue;
    }
    if (subscription == &SWING_GATE_FEED) {
      Serial.print(F("Swing gate command Got: "));
      Serial.println((char *)SWING_GATE_FEED.lastread);
      RF_D1_FLAG = true;
      swingCount++;
      LOCK_FLAG = false;
      if(swingCount == 4){
        LOCK_FLAG = true;
        swingCount = 0;
        if(!GATE_STATUS_FEED.publish("Gate : Locked")){
          Serial.println("Failed");
        }else Serial.println("OK");
        digitalWrite(BUZZER, HIGH);
        delay(2000);
        digitalWrite(BUZZER, LOW);
      }
      continue;
    }
    if (subscription == &BOTH_GATE_FEED) {
      Serial.print(F("Both gate command Got: "));
      Serial.println((char *)BOTH_GATE_FEED.lastread);
      RF_D2_FLAG = true;
      bothCount++;
      LOCK_FLAG = false;
      if(bothCount == 4){
        LOCK_FLAG = true;
        bothCount = 0;
        if(!GATE_STATUS_FEED.publish("Gate : Locked")){
          Serial.println("Failed");
        }else Serial.println("OK");
        
        digitalWrite(BUZZER, HIGH);
        delay(2000);
        digitalWrite(BUZZER, LOW);
      }
      continue;
    }
  // end of MQTT subscriptions
  }

  //Sliding gate control command
  if(RF_D0_FLAG && (!LOCK_FLAG)){
    switch(slideCount){
      case 1:   //sliding gate open command
        Serial.print("Sliding Gate : Openning");
        digitalWrite(SL_CLOSE, HIGH);
        delay(100);
        digitalWrite(SL_CLOSE, LOW);
        buzzOpening(3);
        if(!GATE_STATUS_FEED.publish("Slide : Opening")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 2:   //sliding gate stop command
        Serial.print("Sliding Gate : Stopped");
        digitalWrite(SL_CLOSE, HIGH);
        delay(100);
        digitalWrite(SL_CLOSE, LOW);
        buzzStoping(3);
        if(!GATE_STATUS_FEED.publish("Slide : Stopped")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 3:
        Serial.print("Sliding Gate : Closing");
        digitalWrite(SL_CLOSE, HIGH);
        delay(100);
        digitalWrite(SL_CLOSE, LOW);
        buzzClosing(3);
        if(!GATE_STATUS_FEED.publish("Slide : Closing")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
    }

    //resetting flags
    RF_D0_FLAG  = false;
    RF_D1_FLAG = false;
    RF_D2_FLAG = false;
    RF_D3_FLAG = false;
    VIDEO_DOOR_FLAG = false;

  }

  //swing gate control command
  if(RF_D1_FLAG && (!LOCK_FLAG)){
    switch(swingCount){
      case 1:   //swing gate open command
        Serial.print("Swing Gate : Openning");
        digitalWrite(SW_OPEN, HIGH);
        delay(100);
        digitalWrite(SW_OPEN, LOW);
        buzzOpening(3);
        if(!GATE_STATUS_FEED.publish("Swing : Opening")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 2:   //swing gate stop command
        Serial.print("Swing Gate : Stopped");
        digitalWrite(SW_OPEN, HIGH);
        delay(100);
        digitalWrite(SW_OPEN, LOW);
        buzzStoping(3);
        if(!GATE_STATUS_FEED.publish("Swing : Stopped")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 3: //swing gate closing command
        Serial.print("Swing Gate : Closing");
        digitalWrite(SW_OPEN, HIGH);
        delay(100);
        digitalWrite(SW_OPEN, LOW);
        buzzClosing(3);
        if(!GATE_STATUS_FEED.publish("Swing : Closing")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
    }

    //resetting flags
    RF_D0_FLAG  = false;
    RF_D1_FLAG = false;
    RF_D2_FLAG = false;
    RF_D3_FLAG = false;
    VIDEO_DOOR_FLAG = false;

  }

  //both gate control command
  if(RF_D2_FLAG && (!LOCK_FLAG)){
    switch(bothCount){
      case 1:   //sliding gate open command
        Serial.print("Both Gate : Openning");
        digitalWrite(SW_OPEN, HIGH);
        digitalWrite(SL_OPEN, HIGH);
        delay(100);
        digitalWrite(SL_OPEN, LOW);
        digitalWrite(SW_OPEN, LOW);
        buzzOpening(3);
        if(!GATE_STATUS_FEED.publish("Both : Opening")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 2:   //sliding gate stop command
        Serial.print("Both Gate : Stopped");
        digitalWrite(SW_STOP, HIGH);
        digitalWrite(SL_STOP, HIGH);
        delay(100);
        digitalWrite(SL_STOP, LOW);
        digitalWrite(SW_STOP, LOW);
        buzzStoping(3);
        if(!GATE_STATUS_FEED.publish("Both : Stopped")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 3:
        Serial.print("Both Gate : Closing");
        digitalWrite(SW_CLOSE, HIGH);
        digitalWrite(SL_CLOSE, HIGH);
        delay(10);
        digitalWrite(SL_CLOSE, LOW);
        digitalWrite(SW_CLOSE, LOW);
        buzzClosing(3);
        if(!GATE_STATUS_FEED.publish("Both : Closing")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
    }

    //resetting flags
    RF_D0_FLAG  = false;
    RF_D1_FLAG = false;
    RF_D2_FLAG = false;
    RF_D3_FLAG = false;
    VIDEO_DOOR_FLAG = false;

  }

  //child gate operate command
  if(VIDEO_DOOR_FLAG){
    switch(childGateCount){
      case 1:   //sliding gate open command
        Serial.print("Swing Gate : Openning");
        digitalWrite(SW_OPEN, HIGH);
        delay(100);
        digitalWrite(SW_OPEN, LOW);
        buzzOpening(3);
        if(!GATE_STATUS_FEED.publish("Child : Opening")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 2:   //sliding gate stop command
        Serial.print("Swing Gate : Stopped");
        digitalWrite(SW_STOP, HIGH);
        delay(100);
        digitalWrite(SW_STOP, LOW);
        buzzStoping(3);
        if(!GATE_STATUS_FEED.publish("Child : Stopped")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
      case 3:
        Serial.print("Swing Gate : Closing");
        digitalWrite(SW_CLOSE, HIGH);
        delay(100);
        digitalWrite(SW_CLOSE, LOW);
        buzzClosing(3);
        if(!GATE_STATUS_FEED.publish("Child : Closing")){
          Serial.println("Failed");
        }else Serial.println("OK");
        break;
    }

    //resetting flags
    RF_D0_FLAG  = false;
    RF_D1_FLAG = false;
    RF_D2_FLAG = false;
    RF_D3_FLAG = false;
    VIDEO_DOOR_FLAG = false;

  }

  //remote lock command
  if(RF_D3_FLAG){
    switch(lockCount){
      case 3:   //unlock remote lock
        Serial.println("Unlock Remote");
        LOCK_FLAG = false;
        digitalWrite(BUZZER, HIGH);
        delay(400);
        digitalWrite(BUZZER, LOW);
        delay(300);
        digitalWrite(BUZZER, HIGH);
        delay(400);
        digitalWrite(BUZZER, LOW);
        break;
      case 5:   //locking remote lock
        Serial.println("Lock Remote");
        digitalWrite(BUZZER, HIGH);
        delay(2000);
        digitalWrite(BUZZER, LOW);
        LOCK_FLAG = true;
        break;
    }

    //resetting flags
    RF_D0_FLAG  = false;
    RF_D1_FLAG = false;
    RF_D2_FLAG = false;
    RF_D3_FLAG = false;
    VIDEO_DOOR_FLAG = false;

  }

  // ------------------------ end of the command ----------------
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
