#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  mySerial.begin(9600);
  mySerial.print("AT");

}

void loop() {
  if(mySerial.available()){
    Serial.println(mySerial.readString());
    }
    
}
