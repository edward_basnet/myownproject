
/*
 * (c)Taranga Education
 * This firmware is developed for a educational board of:
 *  - esp6266 breakout board (nodeMCU)
 *  - 4-channel relay
 *  - RGB led
 *  - switches etc.
 * It has different demo modes can be changed by pressing mode switch
 * External AC bulb and on-board RGB led controlling is main purpose
 *  - control using on-board switches 
 *  - control over the internet
 *  - control using sensors(PIR-sensor)
 * Sep 2017
 */

/******************** defining external libraries ********************/
#include <ESP8266WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
//#include <Wire.h>
//#include <LiquidCrystal_I2C.h>

 // creating I2C liquid crystal object and defining I2C address
//LiquidCrystal_I2C lcd(0x27, 16, 2);

/******************** WiFi Access Point ********************/
#define WLAN_SSID     "NISHAN"
#define WLAN_PASS     "111111111"
/******************** Adafruit.io setup ********************/

#define AIO_SERVER      "io.adafruit.com"
#define AIO_SERVERPORT  1883                   // use 8883 for SSL
#define AIO_USERNAME    "devil_edward"
#define AIO_KEY         "e9ee263641034427866027fa1d523394"

// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, AIO_SERVER, AIO_SERVERPORT, AIO_USERNAME, AIO_KEY);

/****************************** Feeds ***************************************/

// Notice MQTT paths for AIO follow the form: <username>/feeds/<feedname>
Adafruit_MQTT_Publish sensePeople = Adafruit_MQTT_Publish(&mqtt, AIO_USERNAME "/feeds/PIR");

// Setup a feed called 'onoff' for subscribing to changes.
Adafruit_MQTT_Subscribe onoffRed = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/RED");
Adafruit_MQTT_Subscribe onoffGreen = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/GREEN");
Adafruit_MQTT_Subscribe onoffBlue = Adafruit_MQTT_Subscribe(&mqtt, AIO_USERNAME "/feeds/BLUE");

/******************** defining pins ********************/
#define MODE 0
#define SW_R 14
#define SW_G 12
#define SW_B 13
#define SW_A 15   //controlling All loads
#define BULB_R 16
#define BULB_G 5
#define BULB_B 4
#define PIR A0
#define LED_R 2 
#define LED_G 1
#define LED_B 3
#define DEBOUNCE_DELAY 500
volatile byte modeCounter = 0;
boolean bulb_r_status = HIGH, bulb_g_status = HIGH, bulb_b_status = HIGH, bulb_rgb_status = HIGH;
boolean led_r_status = LOW, led_g_status = LOW, led_b_status = LOW;
int led_r_value = 0, led_g_value = 0, led_b_value = 0;

void MQTT_connect();

void setup(){
  pinMode(MODE, INPUT_PULLUP);
  pinMode(SW_R, INPUT_PULLUP);
  pinMode(SW_G, INPUT_PULLUP);
  pinMode(SW_B, INPUT_PULLUP);
  pinMode(SW_A, INPUT);
  pinMode(PIR, INPUT);
  pinMode(BULB_R, OUTPUT);
  pinMode(BULB_G, OUTPUT);
  pinMode(BULB_B, OUTPUT);
  pinMode(LED_R, OUTPUT);
  pinMode(LED_G, OUTPUT);
  pinMode(LED_B, OUTPUT);

  digitalWrite(BULB_R, HIGH);
  digitalWrite(BULB_G, HIGH);
  digitalWrite(BULB_B, HIGH);
  analogWrite(LED_R,0);
  analogWrite(LED_G,0);
  analogWrite(LED_B,0);
  
  //I2C lcd initialization
//  lcd.begin(16, 2);
//  lcd.init();
//  lcd.backlight();
  //interrupt operation
  attachInterrupt(digitalPinToInterrupt(MODE), changeMode, FALLING);
  Serial.begin(9600);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
//    lcd.print(".");
  } Serial.println();
//  lcd.setCursor(0,0);
//  lcd.print("connected");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());

  // Setup MQTT subscription for onoff feed.
  mqtt.subscribe(&onoffRed);
  mqtt.subscribe(&onoffGreen);
  mqtt.subscribe(&onoffBlue);
  }

void changeMode(){
  modeCounter++;
  }

void loop(){
  int a = random(0, 255);
  int k = random(0, 255);
  int c = random(0, 255);
  switch(modeCounter){
    case 0:
      /******************** randomize rgb led ********************/
      Serial.println("mode 0");
      analogWrite(LED_R, a);
      analogWrite(LED_G, k);
      analogWrite(LED_B, c);
      delay(DEBOUNCE_DELAY*2);
      break;
    case 1:
      Serial.println("mode 1");
      Bulb_random();
      delay(DEBOUNCE_DELAY * 4);
      Bulb_off();
      break;
    case 2: 
      /******************** controlling on-board RGB led ********************/
      Serial.println("mode 2");
      if(!digitalRead(SW_R)){
        CTRL_Rled();
      }
      if(!digitalRead(SW_G)){
        CTRL_Gled();
      }
      if(!digitalRead(SW_B)){
        CTRL_Bled();
      }
      if(digitalRead(SW_A)){
        digitalWrite(BULB_R, LOW);
        delay(2000);
        digitalWrite(BULB_R, HIGH);
        
      }
      break;
    case 3:
      /******************** controlling AC bulb ********************/ 
      Serial.println("mode 3");
      if(!digitalRead(SW_R)){
        CTRL_Rbulb();
      }
      if(!digitalRead(SW_G)){
        CTRL_Gbulb();
      }
      if(!digitalRead(SW_B)){
        CTRL_Bbulb();
      }
      if(digitalRead(SW_A)){
        CTRL_RGBbulb();
      }
      break;
     case 4: 
      /******************** controlling RGB led from internet ********************/
      Serial.println("mode 4");
      MQTT_connect();
      Adafruit_MQTT_Subscribe *subscription1;
      while ((subscription1 = mqtt.readSubscription(5000))) {
         if (subscription1 == &onoffRed) {
          Serial.print(F("Got: "));
          Serial.println((char *)onoffRed.lastread);
          if((char *)onoffRed.lastread){
            CTRL_Rled();
            }
      }
      if (subscription1 == &onoffGreen) {
        Serial.print(F("Got: "));
        Serial.println((char *)onoffGreen.lastread);
        if((char *)onoffGreen.lastread){
            CTRL_Gled();
            }
      }
      if (subscription1 == &onoffBlue) {
        Serial.print(F("Got: "));
        Serial.println((char *)onoffBlue.lastread);
        if((char *)onoffBlue.lastread){
            CTRL_Bled();
          }
      }
    }

    // Now we can publish stuff!
    if (! sensePeople.publish(analogRead(PIR))) {
      Serial.println(F("Failed"));
    } else {
      Serial.println(F("OK!"));
    }

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  /*
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  */
      break;
    case 5:
      /******************** controlling AC Bulb from internet ********************/ 
      Serial.println("mode 5");
      MQTT_connect();
      Adafruit_MQTT_Subscribe *subscription;
      while ((subscription = mqtt.readSubscription(5000))) {
         if (subscription == &onoffRed) {
          Serial.print(F("Got: "));
          Serial.println((char *)onoffRed.lastread);
          if((char *)onoffRed.lastread){
            CTRL_Rbulb();
            }
      }
      if (subscription == &onoffGreen) {
        Serial.print(F("Got: "));
        Serial.println((char *)onoffGreen.lastread);
        if((char *)onoffGreen.lastread){
            CTRL_Gbulb();
            }
      }
      if (subscription == &onoffBlue) {
        Serial.print(F("Got: "));
        Serial.println((char *)onoffBlue.lastread);
        if((char *)onoffBlue.lastread){
            CTRL_Bbulb();
          }
      }
    }

    // Now we can publish stuff!
    if (! sensePeople.publish(analogRead(PIR))) {
      Serial.println(F("Failed"));
    } else {
      Serial.println(F("OK!"));
    }

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  /*
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  */
      break;
    case 6:
      if(analogRead(PIR)<=500) {
        digitalWrite(BULB_G, LOW);
        digitalWrite(BULB_R, HIGH);
      }
      else {
        digitalWrite(BULB_R, LOW);
        digitalWrite(BULB_G, HIGH);
      }
      break;
    case 7:
      Serial.println("mode 6 \n resetting...");
      modeCounter = 0;
      break;
    default:
      break;
    }
  
  }


/******************** AC bulb/ external load control ********************/
void CTRL_Rbulb(){
  bulb_r_status = !bulb_r_status;
  digitalWrite(BULB_R, bulb_r_status);
  delay(DEBOUNCE_DELAY);
  }
void CTRL_Gbulb(){
  bulb_g_status = !bulb_g_status;
  digitalWrite(BULB_G, bulb_g_status);
  delay(DEBOUNCE_DELAY);
  }
void CTRL_Bbulb(){
  bulb_b_status = !bulb_b_status;
  digitalWrite(BULB_B, bulb_b_status);
  delay(DEBOUNCE_DELAY);
  }
void CTRL_RGBbulb(){
  bulb_rgb_status = !bulb_rgb_status;
  digitalWrite(BULB_R, bulb_rgb_status);
  digitalWrite(BULB_G, bulb_rgb_status);
  digitalWrite(BULB_B, bulb_rgb_status);
  delay(DEBOUNCE_DELAY);
  }
/******************** on-board led control ********************/
void CTRL_Rled(){
  led_r_status = !led_r_status;
  if(led_r_status) led_r_value = 255;
  else led_r_value = 0;
  analogWrite(LED_R, led_r_value);
  analogWrite(LED_G, 0);
  analogWrite(LED_B, 0);
  delay(DEBOUNCE_DELAY);
  }
void CTRL_Gled(){
  led_g_status = !led_g_status;
  if(led_g_status) led_g_value = 255;
  else led_g_value = 0;
  analogWrite(LED_R, 0);
  analogWrite(LED_G, led_g_value);
  analogWrite(LED_B, 0);
  delay(DEBOUNCE_DELAY);
  }
void CTRL_Bled(){
  led_b_status = !led_b_status;
  if(led_b_status) led_b_value = 255;
  else led_b_value = 0;
  analogWrite(LED_R, 0);
  analogWrite(LED_G, 0);
  analogWrite(LED_B, led_b_value);
  delay(DEBOUNCE_DELAY);
  }

void Bulb_random(){
  digitalWrite(BULB_R, boolean(random(0,2)));
  digitalWrite(BULB_G, boolean(random(0,2)));
  digitalWrite(BULB_B, boolean(random(0,2)));
  }
void Bulb_off(){
  digitalWrite(BULB_R, HIGH);
  digitalWrite(BULB_G, HIGH);
  digitalWrite(BULB_B, HIGH);
  }


void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
