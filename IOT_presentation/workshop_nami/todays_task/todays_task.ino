#include <SoftwareSerial.h>

#define RX 13
#define TX 12
SoftwareSerial mySerial(RX, TX);


void setup() {
  //setting software serial baudrate
  mySerial.begin(9600);
  Serial.begin(115200);
}

void loop() {
  while(mySerial.available()){
    Serial.write(mySerial.read());
    }
   while(Serial.available()){
    mySerial.write(Serial.read());
    }
}
