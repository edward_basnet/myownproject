/*
 * (c)Edward
 * Firmware made for IOT system, which is able to receive certain message
 * from other machine to acheive certain task
 *  - SIM808 GPS/GPRS module
 *  - Arduino Uno/nano
 *  - Simcard with data
 *  
 *  Dependent Libraries:
 *    - PubSubClient library
 *         https://github.com/knolleary/pubsubclient/releases/latest
 *        or from http://librarymanager/all#PubSubClient
 *    - tiny-GSM library
 *        http://tiny.cc/tiny-gsm-readme
 *        
 *        Nov 2017
 */

//select your modem
#define TINY_GSM_MODEM_SIM808

/******************** defining external libraries ********************/
#include <SoftwareSerial.h>
#include <TinyGsmClient.h>
#include <PubSubClient.h>


/******************** defining software serial  ********************/
#define RXD 6   //software serial port 
#define TXD 7
SoftwareSerial SerialAT(RXD, TXD);

TinyGsm modem(SerialAT);    //creating tinygsm's object as modem
TinyGsmClient client(modem);  //creating tiny-gsm client's object as client
PubSubClient mqtt(client);    //creating pubsubclient's object as mqtt

/******************** GPRS credentials ********************/
const char APN[]  = "ncell";
const char USER[] = "";
const char PASS[] = "";

/******************** setting mqtt broker ********************/
const char* BROKER_IP = "139.162.2.53";
uint16_t BROKER_PORT = 1883;

const char* TOPIC = "nami/workshop/test";
const char* TOPIC_LED = "nami/workshop/led";

#define LED 13
bool LED_STATUS = LOW;
#define BUZZER 8
bool BUZZER_STATUS = LOW;

long reconnectTimeCounter = 0;
/******************** ********************/

void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUZZER, OUTPUT);
  Serial.begin(115200);   //console baud rate
  delay(10);
  SerialAT.begin(9600);   //GSM module's baudrate
  delay(10);

  //modem initialization, restarting modem is more time consuming
  modem.init();
//  modem.restart();
  Serial.println(". OK");

  String modemInfo = modem.getModemInfo();    //which modem?
  Serial.print("modem: ");
  Serial.println(modemInfo);
    //checking network
  if(!modem.waitForNetwork()){
      //check fail
    Serial.println("Network check failed");
    while(true);
    }Serial.println(".. OK");

    //connecting to GPRS
  if(!modem.gprsConnect(APN, USER, PASS)) {
      //gprs connection failed
    Serial.println("GPRS connection failed");
    while(true);
    }Serial.println("... OK");

      //MQTT setup
    mqtt.setServer(BROKER_IP, BROKER_PORT);
    mqtt.setCallback(mqttCallback);
}

void loop() {
  if(mqtt.connected()) {
      //mqtt ok
//    Serial.println("=/ ");
    mqtt.loop(); 
  }
  else{
      //reconnect in every 10 seconds
    unsigned long attemptTime = millis();  //attempt time store
    Serial.println(attemptTime);
    if((attemptTime - reconnectTimeCounter) > 10000L) {
        //attempting to reconnect to mqtt broker
      Serial.print("reconnecting.");
      reconnectTimeCounter = attemptTime; //updating reconnect time counter
      if(connectMQTT()) {
          //successfully connected to mqtt
        Serial.println("OK");
        reconnectTimeCounter = 0;
        }Serial.print(".");
      }
    }
}

  //MQTT connection
boolean connectMQTT() {
    //connecting to the topics defined earlier
  if(!mqtt.connect(TOPIC)) {
      //topics connection failed
    return false;
    }
    else{
        //topics connection success
      mqtt.subscribe(TOPIC);
      return mqtt.connected();
      }  
  }
  
  //creating mqtt callback function for callback
  //wheneever some message is received callback function gets called
void mqttCallback(char* topic, byte* payload, unsigned int len) {
  Serial.flush(); //flushing serial comm data
    //message arraived
  Serial.write(payload, len);
  Serial.println();
    //proceed if incomming message's topics matches
  if(String(topic) == TOPIC) {
      //extracting value of payload like payload[i]
    if((payload[0] - '0') == 1) {
      LED_STATUS = 1;
      BUZZER_STATUS = 1;
      }
    if((payload[0] - '0') == 0) {
      LED_STATUS = 0;
      BUZZER_STATUS = 0;
      }
      //controlling led and publishing led status over mqtt
    Serial.print("message : ");
    Serial.println(payload[0]);
    digitalWrite(LED, LED_STATUS);
    digitalWrite(BUZZER, BUZZER_STATUS);
//    mqtt.publish(TOPIC_LED, LED_STATUS ? "1" : "0");
}
  }
